'''
Generate pie charts of party vote counts for each electorate
'''

import os
import matplotlib.pyplot as plt
import process_csv
import defines

pie_threshold = 0.025

if __name__ == "__main__":
    results = dict()

    for csv_file in os.listdir("data"):
        result = process_csv.ElectorateResults("data/{}".format(csv_file))
        results[result.electorate] = result

    if not os.path.exists("plots"):
        os.mkdir("plots")

    for electorate, result in results.items():
        pie_segments = dict()
        party_numbers = result.get_party_numbers()
        other_votes = 0
        for k, v in result.get_party_percentages().items():
            if v > pie_threshold:
                pie_segments[k] = party_numbers[k]
            else:
                other_votes += party_numbers[k]

        labels = [x + " ({})".format(party_numbers[x]) for x in pie_segments.keys()]
        if other_votes > 0:
            pie_segments["Other"] = other_votes
        labels.append("Other ({})".format(other_votes))

        colours = [defines.colour[x] for x in pie_segments.keys()]


        plt.pie(pie_segments.values(), labels=labels, colors=colours)
        plt.title("{} ({} votes)".format(electorate, result.total_party_votes))
        plt.savefig("plots/{}.png".format(electorate), bbox_inches='tight')
        plt.clf()
