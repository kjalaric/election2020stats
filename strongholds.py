'''
Voting strongholds - which electorate was the best-performing (percentage-wise) for each party?
'''

import os
import process_csv


if __name__ == "__main__":
    results = dict()

    for csv_file in os.listdir("data"):
        result = process_csv.ElectorateResults("data/{}".format(csv_file))
        results[result.electorate] = result

    # ignoring informal votes, parties are all the same
    parties = result.parties

    for party in parties:
        highest = 0
        stronghold_electorate = None
        for x, v in results.items():
            if v.get_party_percentages()[party] > highest:
                highest = v.get_party_percentages()[party]
                stronghold_electorate = v.electorate

        print("{}: {} - {:.2f}".format(party, stronghold_electorate, highest*100))
