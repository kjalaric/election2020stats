'''
Calculates some advance voting statistics for each party and electorate.
'''

import os
import process_csv

if __name__ == "__main__":
    results = dict()

    for csv_file in os.listdir("data"):
        result = process_csv.ElectorateResults("data/{}".format(csv_file))
        results[result.electorate] = result

    # ignoring informal votes, parties are all the same
    parties = result.parties
    advance_voting_percentage = dict()

    advance_voting_overall = {x: 0 for x in parties}
    votes_overall = {x: 0 for x in parties}

    for electorate, result in results.items():
        advance_voting_percentage[electorate] = {x: 0 for x in parties}
        for x in parties:
            advance_voting_overall[x] += result.advance_party_votes[x]
            votes_overall[x] += result.advance_party_votes[x] + result.election_day_party_votes[x]

            try:
                advance_voting_percentage[electorate][x] = 100.0 * result.advance_party_votes[x] / float(
                    result.advance_party_votes[x] + result.election_day_party_votes[x])
            except ZeroDivisionError:
                advance_voting_percentage[electorate][x] = 0

    for k, v in advance_voting_overall.items():
        advance_voting_overall[k] /= votes_overall[k]

    print("Advance voting across all electorates as a percentage:")
    for k, v in sorted(advance_voting_overall.items(), key=lambda x: x[1], reverse=True):
            print("\t{}: {:.2f}%".format(k, 100.0*v))

    for k, v in advance_voting_percentage.items():
        print(f"\n{k}")
        for kk, vv in sorted(v.items(), key=lambda x: x[1], reverse=True):
            print("\t{} ({:.1f}% - {}): {:.2f}%".format(kk, 100.0*results[k].get_party_percentages()[kk], results[k].get_party_numbers()[kk], vv))

