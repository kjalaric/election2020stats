'''
Class for processing the csvs prior to use in other scripts
'''

advance_line = "Advance Voting Places"
onday_line = "Voting Places"
total_valid_candidate_string = "Total Valid Candidate Votes"
total_informal_candidate_string = "Informal Candidate Votes"
total_valid_party_string = "Total Valid Party Votes"
total_informal_party_string = "Informal Party Votes"

class ElectorateResults(object):
    def __init__(self, filename):
        # candidate advance, candidate on-the-day, party advance, party on-the-day
        candidate = True
        advance = True
        with open(filename, "r", encoding='utf8') as fi:
            lines = list(fi.readlines())
            electorate = lines[0].split(" - ")[0]

            candidates = [x.strip("\"") for x in lines[0].split(",")[2:]]
            candidates = [candidates[2 * i + 1].strip() + " " + candidates[2 * i].strip() for i in
                          range(int((len(candidates) + 1) / 2))][:-1]
            candidates += [total_valid_candidate_string, total_informal_candidate_string]

            advance_candidate_votes = {x: 0 for x in candidates}
            election_day_candidate_votes = {x: 0 for x in candidates}

            for line in lines[2:]:  # one line of header + "advance voting places"
                if line.strip("\n") == advance_line:
                    advance = True
                    # switch to party if candidate
                    if candidate:
                        candidate = False
                    continue
                elif line.strip("\n") == onday_line:
                    advance = False
                    continue
                elif line.split(" - ")[0] == electorate:
                    parties = line.strip().split(",")[2:]
                    advance_party_votes = {x: 0 for x in parties}
                    election_day_party_votes = {x: 0 for x in parties}
                    continue
                else:
                    try:
                        vote_array = [int(x) for x in line.split("\"")[-1].strip().split(",")[1:]]
                    except ValueError:  # Postal Votes Recieved
                        vote_array = [int(x) for x in
                                      line.split("\"")[-1].strip().split(",")[2:]]  # postal, rest homes, etc.
                    if advance:
                        if candidate:
                            for i, x in enumerate(vote_array):
                                advance_candidate_votes[candidates[i]] += x
                        else:
                            for i, x in enumerate(vote_array):
                                advance_party_votes[parties[i]] += x
                    else:
                        if candidate:
                            for i, x in enumerate(vote_array):
                                election_day_candidate_votes[candidates[i]] += x
                        else:
                            for i, x in enumerate(vote_array):
                                election_day_party_votes[parties[i]] += x

        # remove total/informal parties (already done this for candidates)
        parties.remove(total_valid_party_string)
        parties.remove(total_informal_party_string)

        advance_party_votes.pop(total_valid_party_string, None)
        advance_party_votes.pop(total_informal_party_string, None)
        election_day_party_votes.pop(total_valid_party_string, None)
        election_day_party_votes.pop(total_informal_party_string, None)

        self.electorate = electorate
        self.parties = parties
        self.candidates = candidates
        self.advance_party_votes = advance_party_votes
        self.advance_candidate_votes = advance_candidate_votes
        self.election_day_party_votes = election_day_party_votes
        self.election_day_candidate_votes = election_day_candidate_votes

        self.total_advance_party_votes = sum(x for x in advance_party_votes.values())
        self.total_election_day_party_votes = sum(x for x in election_day_party_votes.values())
        self.total_advance_candidate_votes = sum(x for x in advance_candidate_votes.values())
        self.total_election_day_candidate_votes = sum(x for x in election_day_candidate_votes.values())

        self.total_party_votes = self.total_advance_party_votes + self.total_election_day_party_votes
        self.total_candidate_votes = self.total_advance_candidate_votes + self.total_election_day_candidate_votes

    def get_party_percentages(self):
        percentages = {x: 0 for x in self.parties}

        for x in self.parties:
            percentages[x] = (self.advance_party_votes[x] + self.election_day_party_votes[x])/self.total_party_votes

        return percentages

    def get_party_numbers(self):
        numbers = {x: 0 for x in self.parties}

        for x in self.parties:
            numbers[x] = self.advance_party_votes[x] + self.election_day_party_votes[x]

        return numbers