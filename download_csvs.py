'''
Downloads preliminary election results from the internet
'''
import requests
import os

if __name__ == "__main__":
    url_format = "https://electionresults.govt.nz/electionresults_2020_preliminary/statistics/csv/{}_ElectionDay.csv"
    electorates = 72

    if not os.path.exists("data"):
        os.mkdir("data")

    for i in range(1,10):
        url = url_format.format('0{}'.format(i))
        r = requests.get(url)
        with open(os.path.join("data", url.split("/")[-1]), "wb") as fo:
            fo.write(r.content)

    for i in range(10, electorates+1):
        url = url_format.format(i)
        r = requests.get(url)
        with open(os.path.join("data", url.split("/")[-1]), "wb") as fo:
            fo.write(r.content)