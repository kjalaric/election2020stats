'''
Takes the plots and puts them into a .pdf for easier viewing
'''
from fpdf import FPDF
from PIL import Image
import os

if __name__ == "__main__":
    list_pages = os.listdir("plots")
    cover = Image.open("plots/" + list_pages[0])
    width, height = cover.size
    pdf = FPDF(unit="pt", format=[width, height])
    for page in list_pages:
        pdf.add_page()
        pdf.image("plots/{}".format(page), 0, 0)
    pdf.output("Party votes by Electorate.pdf", "F")