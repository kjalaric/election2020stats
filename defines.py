colour = {
    'ACT New Zealand': "yellow",
    'Advance NZ': "grey",
    'Aotearoa Legalise Cannabis Party': "grey",
    'Green Party': "green",
    'HeartlandNZ': "grey",
    'Labour Party': "red",
    'Māori Party': "darkred",
    'National Party': "blue",
    'New Conservative': "cyan",
    'New Zealand First Party': "black",
    'NZ Outdoors Party': "grey",
    'ONE Party': "grey",
    'Social Credit': "grey",
    'Sustainable New Zealand Party': "grey",
    'TEA Party': "grey",
    'The Opportunities Party (TOP)': "springgreen",
    'Vision New Zealand': "grey",

    # for plotting
    'Default': "whitesmoke",
    'Other': "whitesmoke",
}
